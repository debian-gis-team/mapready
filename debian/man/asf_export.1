.TH ASF_EXPORT 1 "December 18, 2010"
.SH NAME
asf_export \- program to export data from ASF internal format
.SH SYNOPSIS
.B asf_export
.RI [-format " <output_format>"]
.RI [-byte " <sample mapping option>"]
.RI [-rgb " <red> <green> <blue>"]
.RI [-band " <band_id | all>"]
.RI [-lut " <look up table file>"]
.RI [-truecolor]
.RI [-falsecolor]
.RI [-log " <log_file>"]
.RI [-quiet]
.RI [-license]
.RI [-version]
.RI [-help]
<in_base_name> <out_full_name>
.SH DESCRIPTION
The program
.B asf_export
ingests ASF internal format data and exports said data to a number of
graphics file formats (TIFF/GEOTIFF, JPEG, PGM, PNG and POlSARPRO).
If the input data was geocoded and the output format supports geocoding,
that information will be included.  Optionally, you may apply look-up
tables, assign color bands (\-rgb, \-truecolor, \-falsecolor).
.PP
.SH OPTIONS
.TP
.B -format <format>
Format to export to. Must be one of the following:

  tiff      - Tagged Image File Format, with byte valued pixels
  geotiff   - GeoTIFF file, with floating point or byte valued pixels
  jpeg      - Lossy compressed image, with byte valued pixels
  pgm       - Portable graymap image, with byte valued pixels
  png       - Portable network graphic, with byte valued pixels
  polsarpro - Flat binary floating point files in PolSARPro format

.B NOTE:
When exporting to a GeoTIFF format file, all map-projection
information is included in GeoKeys as specified in the GeoTIFF
standard.  The other graphics file formats do not support the
storing of map-projection parameters in the output file.  If you
wish to maintain the map-projection and/or georeference (corner
point) information in the output, you should choose the GeoTIFF
output format.

.B NOTE:
When exporting to a GeoTIFF format file, the data format (floating
point, byte, 16-bit integer, etc) will be maintained.  Many viewers
cannot view non-integer data.  Leaving the data format the same as
the original produces the most accurate export, but remapping it to
byte range (0-255) with the \-byte option will result in the greatest
compatibility with viewers and GIS software packages.
.TP
.B -byte <sample mapping option>
Converts output image to byte using the following options:

.B truncate
values less than 0 are mapped to 0, values greater than 255
are mapped to 255, and values in between are converted to
whole numbers (the fractional part of the values are truncated,
not rounded.)

.B minmax
determines the minimum and maximum values of the input image
and linearly maps those values to the byte range of 0 to 255.
The remapping is accomplished using real (floating point)
numbers then the result is converted to a whole number by
truncating the fractional part.

.B sigma
determines the mean and standard deviation of an image and
defines a range of +/- two standard deviations (2-sigma)
around the mean value, and maps this buffer to the byte range
0 to 255 as described for minmax above.  The range limits are
adjusted if the either of the 2-sigma range limits lie outside
the range of the original values.  As with the other remapping
methods, the calculation of values are made with real numbers
and the result is converted to a whole number by truncating
any fractional part.

.B histogram_equalize
develops a look-up table by integrating the (no-adaptation)
whole-image histogram and then normalizing the result to the
0-255 range.  The result is that areas of low contrast, i.e.
flat topography, have a stronger contrast expansion applied
while areas of high contrast, i.e. non-flat topography, will
receive less contrast expansion.  Histogram equalization
is a useful transform for making hard-to-see detail more
visible for the the viewer but is likewise a nonlinear
transform that results in minor (apparent) topography shifts
within the image.  Since shifts occur the most in areas where
the contrast is expanded the most, i.e. flat topography, and
less in areas of more interesting topography, the pragmatic
conclusion is that the nonlinear shifts are quite
insignificant for the majority of users ...only important
if performing precision geography measurements or overlays.
.TP
.B -rgb <red> <green> <blue>
Converts output image into a color RGB image.
<red>, <green>, and <blue> specify which band (channel)
is assigned to color planes red, green, or blue,
ex) "\-rgb HH VH VV", or "\-rgb 3 2 1".  If the word "ignore" is
provided as one or more bands, then the associated color plane
will not be exported, e.g. "\-rgb ignore 2 1" will result in an
RGB file that has a zero in each RGB pixel's red component, band 2
assigned to the green channel, and band 1 assigned to the blue.
The result will be an image with only greens and blues in it.
Currently implemented for GeoTIFF, TIFF, JPEG and PNG.
Cannot be used together with the \-band option.
.TP
.B -lut <look up table file>
Applies a color look up table to the image while exporting.
Only allowed for single-band images.  Images must contain byte (8-bit)
data (except for data deriving from PolSARpro classifications.) Some
look-up table files are in the look_up_tables subdirectory in
the asf_tools share directory.  The tool will look in
this directory for the specified file if it isn't found
in the current directory.
.TP
.B -truecolor
For 3 or 4 band optical satellite images where the first band is the
the blue band, the second green, and the third red.  This option will
export the third band as the red element, the second band as the green
element, and the first band as the blue element.  Performs a 2-sigma
constrast expansion on each individual band during the export (similar
to most GIS software packages.)  To export a true-color image WITHOUT
the contrast expansion associated with the truecolor option, use the
\-rgb option instead.  The rgb option will directly assign the
available bands, unaltered, to the RGB color channels in the output
file, e.g.

    asf_export \-rgb 03 02 01 <infile> <outfile>

Only allowed for multi-band images with 3 or more bands.
The truecolor option cannot be used together with any of the
following options: \-rgb, \-band, or \-falsecolor.
.TP
.B -falsecolor
For 4 band optical satellite images where the second band is green,
the third red, and the fourth is the near-infrared band.  Exports
the fourth (IR) band as the red element, the third band as the green
element, and the second band as the blue element.  Performs a 2-sigma
constrast expansion on the individual bands during the export.  To
export a falsecolor image WITHOUT the contrast expansion, use the
\-rgb flag to directly assign the available bands to the RGB color
channels, e.g.

    asf_export \-rgb 04 03 02 <infile> <outfile>

Only allowed for multi-band images with 4 bands.
Cannot be used together with any of the following: \-rgb, \-band,
or \-truecolor.
.TP
.B -band <band_id | all>
If the data contains multiple data files, one for each band (channel)
then export the band identified by 'band_id' (only).  If 'all' is
specified rather than a band_id, then export all available bands into
individual files, one for each band.  Default is "\-band all".
Cannot be chosen together with the \-rgb option.
.TP
.B -log <logFile>
Output will be written to a specified log file.
.TP
.B -quiet
Supresses all non-essential output.
.TP
.B -license
Print copyright and license for this software then exit.
.TP
.B -version
Print version and copyright then exit.
.TP
.B -help
Print a help page and exit.
.SH EXAMPLES
To export to the default GeoTIFF format from file1.img and file1.meta
to file1.tif:

    asf_export file1 file1

.B NOTE:
When exporting to a GeoTIFF format file, all map-projection
information is included in GeoKeys as specified in the GeoTIFF
standard.

.B NOTE:
When exporting to a GeoTIFF format file, the data format (floating
point, byte, 16-bit integer, etc) will be maintained.  Many viewers
cannot view non-integer data.  Leaving the data format the same as
the original produces the most accurate export, but remapping it to
byte range (0-255) with the \-byte option will result in the greatest
compatibility with viewers and GIS software packages.

To export to file2.jpg in the jpeg format:

    asf_export \-format jpeg file1 file2

.SH LIMITATIONS
Currently supports ingest of ASF format floating point and byte data (only).

Floating-point image formats (i.e., geotiff) are not supported in many
image viewing programs.
.SH SEE ALSO
.BR mapready (1),
.BR asf_mapready (1),
.BR asf_import  (1).
.br
.SH AUTHOR
asf_export was written by University of Alaska Fairbanks, Alaska Satellite Facility.
.PP
This manual page was written by Antonio Valentino <antonio.valentino@tiscali.it>,
for the Debian project (and may be used by others).
