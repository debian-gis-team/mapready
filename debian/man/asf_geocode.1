.TH ASF_GEOCODE 1 "December 18, 2010"
.SH NAME
asf_geocode \- program to interpolate the ODR files
.SH SYNOPSIS
.B asf_geocode
.RI -p "<projection name> <<projection parameters>>"
.RI [-force]
.RI [-resample-method " <method>"]
.RI [-height " <height>"]
.RI [-datum " <datum>"]
.RI [-pixel-size " <pixel size>"]
.RI [-band " <band_id | all>"]
.RI [-log <file>]
.RI [-write-proj-file <file>]
.RI [-read-proj-file <file>]
.RI [-save-mapping]
.RI [-background <value>]
.RI [-quiet]
.RI [-license]
.RI [-version]
.RI [-help]
<in_base_name> <out_base_name>
.SH DESCRIPTION
The program
.B asf_geocode
takes a map projected or an unprojected (ground range) image in the ASF
internal format and geocodes it, i.e. swizzles it around into one of the
standard projections used for maps (universal transverse mercator, polar
stereo, etc).  The output is a new image in ASF internal format.
Most of the "options" are actually required.  The specification of a
certain projection type implies that all the parameters required to
fully specify a projection of that type be included.
This must be an ASF internal format image base name.
The base name of the geocoded image to produce.
.PP

.SH PROJECTION PARAMETER OPTIONS
All these options take arguments, unless otherwise noted.  Groups
of options which appear together on a single line separated by
commas are aliases for the same parameter (they are intended to
aid recognition, since there is much confusion of map projection
terminology).
.TP
.B  -p, -projection <projection>
Projection to use.  Argument must be one of the following:

    utm    - Universal Transverse Mercator
    ps     - Polar stereographic
    lamcc  - Lambert conformal conic
    lamaz  - Lambert azimuthal equal area
    albers - Albers conical equal area
.TP

.B UTM
.TP
.B --zone [<zone>]
If a zone is not specified, it will be determined from the scene's metadata.
.TP

.B POLAR STEREO
.TP
.B --first-standard-parallel
Latitude of True Scale
.TP
.B --central-meridian
Longitude of Central Meridian
.TP
.B --north-pole
Center on North Pole (no argument)
.TP
.B --south-pole
Center on South Pole (no argument)
.TP
.B --false-easting
False Easting (optional)
.TP
.B --false-northing
False Northing (optional)
.TP

.B LAMBERT CONFORMAL CONIC
.TP
.B --first-standard-parallel
First Standard Parallel
.TP
.B --second-standard-parallel
Second Standard Parallel
.TP
.B --latitude-of-origin
Latitude at projection"s origin
.TP
.B --central-meridian
Central Meridian
.TP
.B --false-easting
False Easting (optional)
.TP
.B --false-northing
False Northing (optional)
.PP
You may omit the origin (the image center will be used as the origin),
however the standard parallels are required.
.TP

.B LAMBERT AZIMUTHAL EQUAL AREA
.TP
.B --latitude-of-origin
Latitude at center of projection
.TP
.B --central-meridian
Longitude at center of projection
.TP
.B --false-easting
False Easting (optional)
.TP
.B --false-northing
False Northing (optional)
.PP
You may omit the point of tangency (the image center will be used).
.TP

.B ALBERS CONICAL EQUAL AREA
.TP
.B --first-standard-parallel
First Standard Parallel
.TP
.B --second-standard-parallel
Second Standard Parallel
.TP
.B --latitude-of-origin
Latitude of projection"s origin
.TP
.B --central-meridian
Central Meridian
.TP
.B --false-easting
False Easting (optional)
.TP
.B --false-northing
False Northing (optional)
.PP
You may omit the latitude-of-origin (the latitude at the image center
will be used), however the standard parallels and central meridian are
required.
.TP

.B Projection Option Abbreviations
.TP
The following are accepted as abbreviations for the above options:

    \--first-standard-parallel  : \-plat1
    \--second-standard-parallel : \-plat2
    \--central-meridian         : \-lon0
    \--latitude-of-origin       : \-lat0
    \--north-pole               : \-n
    \--south-pole               : \-s
    \--zone                     : \-z
.PP
For example, instead of:
    \--projection albers \--first-standard-parallel 55
    \--second-standard-parallel 65 \--central-meridian \-154
    \--latitude-of-origin 50

you may use:

    \-p albers \-plat1 55 \-plat2 65 \-lon0 \-154 \-lat0 50

.TP

.SH USING A PROJECTION PARAMETERS FILE
.TP
.B -write-proj-file <file>
Save the specified projection information to a file with
the given name.  The file may be used for subsequent projections
with \--read-proj-file.
.TP
.B -read-proj-file <file>
Read projection information from the given file.  The format of
the file must match what is used with \--write-proj-file.
This option may not be used together with any other projection options.

You cannot specify a projection (with \--projection), and \--read-proj-file
at the same time.
.TP

.SH OTHER OPTIONS
.TP
.B -h, -height <height>
Assume that terrain in the image is <height> meters above
the reference GEM6 ellipsoid.  Optimal geolocation accuracy
will then be achieved for pixels on terrain at this height.
The geolocation of terrain at other height will be off by
about the height difference between the terrain and
<height>, assuming a satellite look angle 45 degrees from
horizontal.
.TP
.B -resample-method <method>
Specifies which interpolation method to use when resampling
images into projection geometry.  Available choices are:
    nearest_neighbor
    bilinear
    bicubic
.TP
.B -datum <datum>
Specifies the datum that is used when projecting.  The datum
applies to the target coordinate system.  Supported Datums:
    NAD27  (North American Datum 1927) (Clarke 1866)
    NAD83  (North American Datum 1983) (GRS 1980)
    WGS84  (World Geodetic System 1984) (default)
    HUGHES (Hughes Reference Spheroid 1980, by the Hughes Aircraft Company)

.B NOTE:
The Hughes reference spheroid is only valid when used with a polar
stereographic projection.

.B NOTE:
When specifying a datum together with a UTM projection type, note the
following valid zone numbers which may be combined with each datum type:
    NAD27  Valid zones are 2 through 22
    NAD83  Valid zones are 2 through 23
    WGS84  Valid zones are 1 through 60
    HUGHES Not valid for any zone

.B NOTE:
When specifying a datum together with a non-UTM projection type, the
following are the valid regions may be combined with each datum type:
    NAD27  North America (only)
    NAD83  North America (only)
    WGS84  Any region
    HUGHES Polar regions
.TP
.B -ps, -pix, -pixel-size <pixel spacing>
Specifies the pixel spacing of the geocoded image.  By default,
the pixel size of the first input image is used.
.TP
.B -band <band_id | all>
If the image file contains multiple bands (channels), then
geocode the band identified by 'band_id' (only) into a single-band
ASF-format file.  If 'all' is specified rather than a band_id, then
geocode all available bands into a single multi-band ASF-format file.
In this case, all bands will be geocoded using the same set of
projection parameters, and the parameters in the resulting metadata
file apply to all.  Default is "\-band all".
.TP
.B -background <background fill value>
Value to use for pixels that fall outside of the scene.  By default,
the outside is filled with zeroes.
.TP
.B -f, -force
Override the built-in projection sanity checks.  Normally,
the program will abort with an error if it detects that a
scene lies in an area where the selected projection is
going to give poor results.  However, you may still wish
to do the projection anyway (such as when you will mosaic
the result a number of other scenes and wish to have them
all in the same projection), the \-force option can be used
in these situations.

.B NOTE:
There are some circumstances that will result in insurmountable errors
in spite of the \-force flag being utilized.  One example is attempting
to use a NADxx (North American) datum in an area outside of North
America will result in projection library errors regardless.
.TP
.B -save-mapping
Creates two additional files during geocoding.  One will contain the
line number from which the pixel was obtained from the original file,
the other the sample numbers.  Together, these define the mapping of
pixels performed by the geocoding.
.TP
.B -log <log file>
Output will be written to a specified log file.
.TP
.B -quiet
Supresses all non-essential output.
.TP
.B -license
Print copyright and license for this software then exit.
.TP
.B -version
Print version and copyright then exit.
.TP
.B -help
Print a help page and exit.
.SH EXAMPLES
To map project an image with centerpoint at \-147 degrees
longitude and average height 466 meters into universal transverse
mercator projection, with one pixel 50 meters on a side:

    asf_geocode \-p utm \--central-meridian \-147.0 \--height 466
                input_image output_image

To geocode one band within an image file, you specify the selected band
with the \-band option, and the selected band MUST be one of which appears
in the list of available bands as noted in the 'bands' item found in the
"general" (first) block in the metadata file.  For example, if 'bands'
contains "01,02,03,04", then you could specify a band_id, e.g. "\-band 02"
etc on the command line.  The same applies to band lists, such
as "HH,HV,VH,VV" or just "03" etc.

    asf_geocode \-p utm \-band HV file outfile_HV

.SH LIMITATIONS
 May fail badly if bad projection parameters are supplied for the area
 in the image.
.SH SEE ALSO
.BR mapready (1),
.BR asf_import (1),
.BR asf_export (1).
.br
.SH AUTHOR
asf_geocode was written by University of Alaska Fairbanks, Alaska Satellite Facility.
.PP
This manual page was written by Antonio Valentino <antonio.valentino@tiscali.it>,
for the Debian project (and may be used by others).
