.TH ASF_TERRCORR 1 "December 18, 2010"
.SH NAME
asf_terrcorr \- program to perform radar imagery ortho-rectification
.SH SYNOPSIS
.B asf_terrcorr
.RI [-log " <logfile>"]
.RI [-quiet]
.RI [-keep (-k)]
.RI [-no-resample]
.RI [-no-interp]
.RI [-pixel-size <size>]
.RI [-mask-file " <filename> " | -auto-water-mask]
.RI [-mask-height-cutoff " <height in meters>"]
.RI [-fill " <fill value> | -no-fill"]
.RI [-update-original-meta (-u)]
.RI [-other-file " <basename>"]
.RI [-do-radiometric]
.RI [-smooth-dem-holes]
.RI [-no_matching]
.RI [-offsets " <range> <azimuth>"]
.RI [-use-zero-offsets-if-match-fails]
<in_base_name> <dem_base_name> <out_base_name>
.SH DESCRIPTION
The program
.B asf_terrcorr
takes two inputs: (1) An unprojected SAR image in the ASF internal
format, and (2) a geocoded digital elevation model (DEM) encompassing
the area in the SAR image, also in the ASF internal format.
The output is an image that has been corrected for the distortions
introduced by the side-looking geometry and the local topography.

Two input files are required.  Each input file, the SAR image and
the DEM, need to be in the ASF internal format, and you should just
specify the basename (i.e., do not give the file's extension) of each.
The input SAR image can be in either slant range or ground range.  If
it is in ground range, it will be converted to slant range.

Alternatively, instead of a DEM you may provide a directory containing
DEMs (in ASF Internal Format, with both .img and .meta files).
In this case, a suitable DEM will be selected from the DEMs in the
directory (including all subdirectories, recursively searched), or
one will be composed (i.e., mosaicked) from the DEMs in the given
directory.  All DEMs covering the given SAR scene are included in
the mosaic.

The base name of the terrain corrected image to produce.  The output
image is in ground range.

A layover/shadow mask of the region will also be automatically produced
and it will have the same base name as the output file, with the
suffix '_mask' added.

The layover shadow mask values are as follows:

    Normal: 1
    User Masked: 2
    Shadow: 3
    Layover: 4
    Invalid: 5

You may export the layover/shadow mask using asf_export to produce
a color-coded mask, by using the "\-lut layover_mask" option with
asf_export.
.PP
.SH OPTIONS
.TP
.B t0
epoch for the orbit interpolation in various date formats.
.TP
.B -k, -keep
The terrain correction process produces a number of intermediate
files on the way to generating the final product.  Normally, these
temporary files are deleted when the process completes, however
if you wish to keep these files around you may do so with this
option.
.TP
.B -no-resample
If the DEM has a pixel size that is significantly larger (a factor
of 2) than the SAR image, by default the SAR image is downsampled
to a pixel size half that of the DEM.  With \-no-resample, no
resampling of this type will be done.  However, the quality of the
terrain corrected product is still limited by the resolution of
the DEM.
.TP
.B -no-interp
Layover regions in the output image are going to contain data that
is not scientifically valid.  With this option, those regions will be
left blank instead of filled with interpolated values.

If you are planning to analyze image statistics, you will probably
want to use this option, even though the results won't look as
pretty.
.TP
.B -pixel_size <pixel spacing>
Specifies the pixel spacing of the terrain corrected image.
asf_terrcorr by default will preserve the pixel size of the input
image (however, in cases where the DEM and SAR resolution differ by
more than a factor of two your image may be automatically
downsampled).
.TP
.B -use-gr-dem
.TP
.B -use-sr-dem
The DEM that is provided to asf_terrcorr is generally in ground
range, and is converted to slant range as part of the coregistration
process.  After coregistration, you can use either the original
ground range DEM, or the slant range DEM, to do the actual terrain
correction.  By default, the slant range DEM is used, specifying
\-use-gr-dem will use the ground range DEM.

The choice only makes a significant difference in regions of layover.
(And in those regions, the difference is quite significant!)

Both produce similar results in areas without layover.  In regions
of layover, however the results are quite different; using the slant
range DEM results in more aggressive interpolations, which sometimes
results in streaky-looking layover regions, whereas using the ground
range DEM preserves more structure within the layover regions, but
looks worse if the coregistration is off in those areas.
.TP
.B -mask-file <filename>
Specify a file that contains regions to be omitted from terrain
correction.  These regions will also not be considered when
attempting to correlate the SAR and DEM images.  Since accurate
correlation is essential for a successful terrain correction,
images with large featureless regions (such as water, or glaciers)
may not correlate well, and therefore may not terrain correct very
well, either.  You may use this option to mask those regions,
confining the correlation to areas that you expect to provide good
matches.

The mask file should be 0 in the unmasked regions, and positive in
the masked regions.

By default, the output image is left blank (filled with zeros) in the
regions that are masked.  You may override this behavior using the
\-fill or \-no-fill options, below.

You cannot use this option together with \-auto-water-mask.
.TP
.B -auto-water-mask
A mask file (see the \-mask-file option, above) is automatically
produced from the DEM.  Areas where the DEM height is less than 1
meter are masked.  (Though you may change this cutoff with the
option \-mask-height-cutoff.)

You cannot use this option together with \-mask-file.
.TP
.B -mask-height-cutoff <height>
Only applies to automatic water masking.

DEM pixels of the given value or below are masked.  The default
is 1 meter.
.TP
.B -fill <fill value>
Specifies a particular value to put in the terrain corrected image
where the mask file (specified with \-mask-file) indicates that the
pixel is masked.  By default, the fill value is 0.
.TP
.B -no-fill
With this option, instead of filling the masked regions (specified
with \-mask-file) with a constant fill value, the masked pixels will
be filled with pixel values from the original SAR image.

This may not work particularly well when the masked regions have high
elevation.
.TP
.B -do-radiometric
Apply radiometric terrain correction.  Radiometric terrain correction
is still experimental.  Currently, this option scales values by
sin(li)/sin(ellips), where li is the local incidence angle (using the
DEM), and ellips is the incidence angle relative to the ellipsoid.
In the future we expect to support more correction formulae.
.TP
.B -smooth-dem-holes
Some DEMs have holes in them, this is particularly a problem with
SRTM DEMs.  If this is the case, the terrain corrected product will
often contain streaks in the areas near the holes.  You can fill
the holes with interpolated values prior to terrain correction with
this option, which will reduce or even eliminate these streaks.
However, you should still expect suboptimal results within the
holes.
.TP
.B -update-original-meta (-u)
The correlation process that is done during the terrain correction
process produces offsets in range and azimuth that, when applied
to the SAR image's metadata, result in a good match with the DEM.
When specifying this option, the input SAR image's metadata is
updated with these offsets, increasing the accuracy of the original
data.
.TP
.B -other-file <basename>
The correlation process that is done during the terrain correction
process produces offsets in range and azimuth that, when applied
to the SAR image's metadata, result in a good match with the DEM.
Sometimes, you will want these same offsets applied to other data
files.  Specifying \-other-file will update the metadata for this
listed file with the calculated offsets, in addition to the files
already updated (specifically, the terrain corrected product, and,
if you specified \-u, the input SAR image).

This option may be specified more than once.
.TP
.B -no-match
Don't use the DEM for matching.
.TP
.B -offsets <range offset> <azimuth offset>
Use these offsets instead of matching the DEM to the SAR image.
The offsets are in pixels.
.TP
.B -use-zero-offsets-if-match-fails
Try to use the DEM to refine the geolocation of the SAR image
before terrain correcting, but if this fails proceed with terrain
correction using offsets of zero.  (I.e., assume the geolocation
of the SAR image is good enough for terrain correction.)  This
often produces good results, especially if the terrain is fairly
flat (a situation that often results in poor or failed
coregistration).

Normally, if the coregistration fails, asf_terrcorr will abort
with an error.
.TP
.B -no-speckle
When generating the simulated SAR image for co-registration, do
not add speckle.  Generally, this will cause co-registration to
fail; usually, you would do this only if you are interested only
in the simulated image itself (and not the terrain corrected
product).  So, processing will stop after generating the simulated
SAR image.
.TP
.B -log <log file>
Output will be written to a specified log file.
.TP
.B -quiet
Supresses all non-essential output.
.TP
.B -license
Print copyright and license for this software then exit.
.TP
.B -version
Print version and copyright then exit.
.TP
.B -help
Print a help page and exit.
.SH EXAMPLES
Terrain correction using default options:

    asf_terrcorr input_image dem_image output_image

Terrain correct with an output image pixel size of 30 meters:

    asf_terrcorr \-pixel-size 30 input_image dem_image output_image

Update the metadata for input_image to correct geolocation errors:

    asf_terrcorr \-u input_image dem_image output_image

Ignore water regions during terrain correction:

    asf_terrcorr \-auto-water-mask input_image dem_image output_image

.SH LIMITATIONS
Can be quite slow when terrain correcting with a mask.
.SH SEE ALSO
.BR mapready (1),
.BR refine_geolocation (1).
.br
.SH AUTHOR
asf_terrcorr was written by University of Alaska Fairbanks, Alaska Satellite Facility.
.PP
This manual page was written by Antonio Valentino <antonio.valentino@tiscali.it>,
for the Debian project (and may be used by others).
