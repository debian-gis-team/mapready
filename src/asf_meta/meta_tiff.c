#include "asf_meta.h"
#include <geokeys.h>
#include <geo_tiffp.h>
#include <geo_keyp.h>
#include <geotiff.h>
#include <geotiffio.h>
#include <tiff.h>
#include <tiffio.h>
#include <xtiffio.h>
#include <geotiff_support.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#define TIFFTAG_ASF_INSAR_METADATA  42112

static xmlXPathObjectPtr
getnodeset(xmlDocPtr doc, xmlChar *xpath){
	
  xmlXPathContextPtr context;
  xmlXPathObjectPtr result;
  
  context = xmlXPathNewContext(doc);
  if (context == NULL) {
    printf("Error in xmlXPathNewContext\n");
    return NULL;
  }
  result = xmlXPathEvalExpression(xpath, context);
  xmlXPathFreeContext(context);
  if (result == NULL) {
    printf("Error in xmlXPathEvalExpression\n");
    return NULL;
  }
  if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
    xmlXPathFreeObject(result);
    printf("No result\n");
    return NULL;
  }
  return result;
}

static int get_meta_xml_item(xmlDocPtr doc, xmlChar *parameter)
{
  int ii, found;
  xmlNodeSetPtr keyNode;
  xmlXPathObjectPtr key;
  xmlChar *keyword;
  key = getnodeset(doc, (xmlChar*)"/GDALMetadata/Item/@name");
  if (key) {
    keyNode = key->nodesetval;
    for (ii=0; ii<keyNode->nodeNr; ii++) {
      keyword = 
	xmlNodeListGetString(doc, keyNode->nodeTab[ii]->xmlChildrenNode, 1);
      if (strcmp_case((char *)keyword, (char *)parameter) == 0)
	found = ii;
      xmlFree(keyword);
    }
  }
  return found;
}

static xmlChar *get_meta_xml_units(xmlDocPtr doc, xmlChar *parameter)
{
  xmlNodeSetPtr unitsNode;
  xmlXPathObjectPtr units;
  int item = get_meta_xml_item(doc, parameter);
  units = getnodeset(doc, (xmlChar*)"/GDALMetadata/Item/@units");
  if (units) {
    unitsNode = units->nodesetval;
    return
      xmlNodeListGetString(doc, unitsNode->nodeTab[item]->xmlChildrenNode, 1);
  }
  return NULL;
}

static double get_meta_xml_double(xmlDocPtr doc, xmlChar *parameter)
{
  xmlNodeSetPtr valNode;
  xmlXPathObjectPtr val;
  xmlChar *value;
  int item = get_meta_xml_item(doc, parameter);
  val = getnodeset(doc, (xmlChar*)"/GDALMetadata/Item");
  if (val) {
    valNode = val->nodesetval;
    value = 
      xmlNodeListGetString(doc, valNode->nodeTab[item]->xmlChildrenNode, 1);
    return
      atof((char *)value);
  }
  return MAGIC_UNSET_DOUBLE;
}

static char *get_meta_xml_string(xmlDocPtr doc, xmlChar *parameter)
{
  xmlNodeSetPtr valNode;
  xmlXPathObjectPtr val;
  int item = get_meta_xml_item(doc, parameter);
  val = getnodeset(doc, (xmlChar*)"/GDALMetadata/Item");
  if (val) {
    valNode = val->nodesetval;
    return
      (char *) xmlNodeListGetString(doc, 
				    valNode->nodeTab[item]->xmlChildrenNode, 1);
  }
  return MAGIC_UNSET_STRING;
}

/**
 * Parse the in memory document and free the resulting tree
 */
static xmlDocPtr
get_insar_xml_tree_from_string(const char *document) {
    xmlDocPtr doc;

    /*
     * The document being in memory, it have no base per RFC 2396,
     * and the "noname.xml" argument will serve as its base.
     */
    doc = xmlReadMemory(document, strlen(document), "noname.xml", NULL, 0);
    if ( NULL == doc ) {
        asfPrintStatus("Failed to parse in-memory XML metadata document for InSAR product.\n");
    }
    return doc;
}

/**
 * gotchas: xmlDocPtr is passed by reference
 * returns: bool success
 */
static int
get_insar_xml_from_tiff_tag(const char *tiff_name, xmlDocPtr *doc)
{
    TIFF *tiff = NULL;
    char *insar_xml;
    
    asfPrintStatus("Checking for ASF InSAR metadata from GDAL metadata tag...");
    tiff = XTIFFOpen(tiff_name, "r");
    if (tiff) {
        if ( 0 == TIFFGetField(tiff, TIFFTAG_ASF_INSAR_METADATA, &insar_xml) )
        {
            asfPrintStatus("...didn't find it.\n");
            return FALSE;
        } else {
            asfPrintStatus("...found it!\n");
            *doc = get_insar_xml_tree_from_string(insar_xml);
            return TRUE;
        }   
        XTIFFClose(tiff);
    }
    return FALSE;
}

/**
 * assumptions: the metadata isn't more than 2kb in size.
 * precondition: meta_parameters meta is initialized
 * postcondition: meta->insar has a reference to a valid InSAR data structure
 *     (could be empty).
 */
meta_insar *populate_insar_metadata(const char *filename)
{
  meta_insar *insar = NULL;
  //LIBXML_TEST_VERSION - Check for version/compilation/libraries
  xmlDocPtr doc = NULL;
  
  if ( FALSE == get_insar_xml_from_tiff_tag(filename, &doc) ) {
      asfPrintStatus("Didn't find an InSAR metadata in embedded TIFF metadata.\n");
  } 
  else {
    insar = meta_insar_init();
    strcpy(insar->processor, 
	   get_meta_xml_string(doc, (xmlChar *)"INSAR_PROCESSOR"));
    strcpy(insar->master_image, 
	   get_meta_xml_string(doc, (xmlChar *)"INSAR_MASTER_IMAGE"));
    strcpy(insar->slave_image, 
	   get_meta_xml_string(doc, (xmlChar *)"INSAR_SLAVE_IMAGE"));
    strcpy(insar->master_acquisition_date, 
	   get_meta_xml_string(doc, 
			       (xmlChar *)"INSAR_MASTER_ACQUISITION_DATE"));
    strcpy(insar->slave_acquisition_date, 
	   get_meta_xml_string(doc, 
			       (xmlChar *)"INSAR_SLAVE_ACQUISITION_DATE"));
    insar->center_look_angle = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_CENTER_LOOK_ANGLE");
    strcpy(insar->center_look_angle_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_CENTER_LOOK_ANGLE"));
    insar->doppler = get_meta_xml_double(doc, (xmlChar *)"INSAR_DOPPLER");
    strcpy(insar->doppler_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_DOPPLER"));
    insar->doppler_rate = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_DOPPLER_RATE");
    strcpy(insar->doppler_rate_units, 
	   (char *) get_meta_xml_units(doc, (xmlChar *)"INSAR_DOPPLER_RATE"));
    insar->baseline_length = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_LENGTH");
    strcpy(insar->baseline_length_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_BASELINE_LENGTH"));
    insar->baseline_parallel = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_PARALLEL");
    strcpy(insar->baseline_parallel_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_BASELINE_PARALLEL"));
    insar->baseline_parallel_rate = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_PARALLEL_RATE");
    strcpy(insar->baseline_parallel_rate_units, 
	   (char *) get_meta_xml_units(doc, 
			       (xmlChar *)"INSAR_BASELINE_PARALLEL_RATE"));
    insar->baseline_perpendicular = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_PERPENDICULAR");
    strcpy(insar->baseline_perpendicular_units, 
	   (char *) get_meta_xml_units(doc, 
			       (xmlChar *)"INSAR_BASELINE_PERPENDICULAR"));
    insar->baseline_perpendicular_rate = 
      get_meta_xml_double(doc, 
			  (xmlChar *)"INSAR_BASELINE_PERPENDICULAR_RATE");
    strcpy(insar->baseline_perpendicular_rate_units, 
	   (char *) get_meta_xml_units(doc, 
			  (xmlChar *)"INSAR_BASELINE_PERPENDICULAR_RATE"));
    insar->baseline_temporal = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_TEMPORAL");
    strcpy(insar->baseline_temporal_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_BASELINE_TEMPORAL"));
    insar->baseline_critical = 
      get_meta_xml_double(doc, (xmlChar *)"INSAR_BASELINE_CRITICAL");
    strcpy(insar->baseline_critical_units, 
	   (char *) get_meta_xml_units(doc, 
				       (xmlChar *)"INSAR_BASELINE_CRITICAL"));
      
    xmlFreeDoc(doc);
    xmlCleanupParser();
  }
  return insar;
}
